//
//  CameraCaptureProtocol.swift
//  CustomCameraDemo
//
//  Created by Daffomac-05 on 08/08/16.
//  Copyright © 2016 Daffomac-05. All rights reserved.
//

import UIKit

protocol CameraCaptureProtocol{
    func cameraSessionConfigurationDidComplete();
}
