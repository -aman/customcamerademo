//
//  Camera.swift
//  CustomCameraDemo
//
//  Created by Daffomac-05 on 08/08/16.
//  Copyright © 2016 Daffomac-05. All rights reserved.
//

import UIKit
import Foundation
import AVFoundation

class CameraPresenter : NSObject,CameraPresenterProtocol{
    var responseDelegate : CameraCaptureProtocol?
    lazy var captureSession: AVCaptureSession = AVCaptureSession()
    var sessionQueue: dispatch_queue_t!
    var stillImageOutput: AVCaptureStillImageOutput?
    
    init(responseDelegate : AnyObject){
        super.init()
        self.responseDelegate = responseDelegate as? CameraCaptureProtocol
        self.initializeCamera()
    }
    
    func initializeCamera(){
        captureSession.sessionPreset = AVCaptureSessionPresetPhoto
        self.captureSession = AVCaptureSession()
        self.captureSession.sessionPreset = AVCaptureSessionPresetPhoto
        self.sessionQueue = dispatch_queue_create("camera session", DISPATCH_QUEUE_SERIAL)
        
        dispatch_async(self.sessionQueue) {
            self.captureSession.beginConfiguration()
            self.addVideoInput()
            self.addStillImageOutput()
            self.captureSession.commitConfiguration()
            
            dispatch_async(dispatch_get_main_queue()) {
                print("Session initialization did complete")
                self.responseDelegate?.cameraSessionConfigurationDidComplete()
            }
        }

    }
    
    func startCamera() {
        dispatch_async(self.sessionQueue) {
            self.captureSession.startRunning()
        }
    }
    func stopCamera(){
        dispatch_async(self.sessionQueue) {
            self.captureSession.stopRunning()
        }
    }
    
    func captureStillImage(completed: (image: UIImage?) -> Void) {
        if let imageOutput = self.stillImageOutput {
            dispatch_async(self.sessionQueue, { () -> Void in
                
                var videoConnection: AVCaptureConnection?
                for connection in imageOutput.connections {
                    let c = connection as! AVCaptureConnection
                    
                    for port in c.inputPorts {
                        let p = port as! AVCaptureInputPort
                        if p.mediaType == AVMediaTypeVideo {
                            videoConnection = c;
                            break
                        }
                    }
                    
                    if videoConnection != nil {
                        break
                    }
                }
                
                if videoConnection != nil {
                    imageOutput.captureStillImageAsynchronouslyFromConnection(videoConnection, completionHandler: { (imageSampleBuffer: CMSampleBufferRef!, error) -> Void in
                        let imageData = AVCaptureStillImageOutput.jpegStillImageNSDataRepresentation(imageSampleBuffer)
                        let image: UIImage? = UIImage(data: imageData!)!
                        
                        dispatch_async(dispatch_get_main_queue()) {
                            completed(image: image)
                        }
                    })
                } else {
                    dispatch_async(dispatch_get_main_queue()) {
                        completed(image: nil)
                    }
                }
            })
        } else {
            completed(image: nil)
        }
    }
    

    
    func addVideoInput() {
        var device: AVCaptureDevice?
        device = self.deviceWithMediaTypeWithPosition(AVMediaTypeVideo, position: AVCaptureDevicePosition.Back)
        do {
            let input = try AVCaptureDeviceInput(device: device)
            if self.captureSession.canAddInput(input) {
                self.captureSession.addInput(input)
                
            }
        } catch {
            print(error)
        }
    }
    
    func addStillImageOutput() {
        stillImageOutput = AVCaptureStillImageOutput()
        stillImageOutput?.outputSettings = [AVVideoCodecKey: AVVideoCodecJPEG]
        
        if self.captureSession.canAddOutput(stillImageOutput) {
            captureSession.addOutput(stillImageOutput)
        }
    }
    
    func deviceWithMediaTypeWithPosition(mediaType: NSString, position: AVCaptureDevicePosition) -> AVCaptureDevice? {
        let devices: NSArray = AVCaptureDevice.devicesWithMediaType(mediaType as String)
        var captureDevice: AVCaptureDevice?
        if devices.count > 0{
            captureDevice = devices.firstObject as? AVCaptureDevice
            for device in devices {
                let d = device as! AVCaptureDevice
                if d.position == position {
                    captureDevice = d
                    break;
                }
            }
            return captureDevice!
        }
        else{
            return nil
        }
        
    }
}