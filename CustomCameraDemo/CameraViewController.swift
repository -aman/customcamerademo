//
//  CameraViewController.swift
//  CustomCameraDemo
//
//  Created by Daffomac-05 on 08/08/16.
//  Copyright © 2016 Daffomac-05. All rights reserved.
//

import UIKit
import AVFoundation

class CameraViewController: UIViewController,CameraCaptureProtocol {
    var camera :CameraPresenter?
    var preview : AVCaptureVideoPreviewLayer?
    lazy var cameraStill : UIImageView = UIImageView()
    @IBOutlet weak var btnCapture: UIButton!
    @IBOutlet weak var btnReset: UIButton!
    override func viewDidLoad() {
        super.viewDidLoad()
        camera = CameraPresenter(responseDelegate: self)
        establishVideoPreviewArea()
        self.cameraStill = UIImageView(frame: CGRect(x: 0,y: 0,width: self.view.frame.width,height: self.view.frame.height))
        self.view.bringSubviewToFront(btnCapture)

        // Do any additional setup after loading the view.
    }

    @IBAction func captureButtonClicked(sender: UIButton) {
        
            self.camera?.captureStillImage({ (image) -> Void in
                if image != nil {
                    self.cameraStill.contentMode = .ScaleAspectFill
                    self.cameraStill.image = image;
                    self.view.addSubview(self.cameraStill)
                    self.view.bringSubviewToFront(self.btnReset)
                } else {
//                    self.cameraStatus.text = "Uh oh! Something went wrong. Try it again."
//                    self.status = .Error
                }
                
            })
    }
    
    @IBAction func resetButtonClicked(sender: UIButton) {
        cameraStill.removeFromSuperview()
    }
    func establishVideoPreviewArea() {
        self.preview = AVCaptureVideoPreviewLayer(session: self.camera!.captureSession)
        self.preview?.videoGravity = AVLayerVideoGravityResizeAspectFill
        self.preview?.frame = self.view.bounds
        self.view.layer.addSublayer(self.preview!)
    }
    
    func cameraSessionConfigurationDidComplete(){
        self.camera?.startCamera()
    }
    
//    func cameraSessionDidBegin() {
//        self.cameraStatus.text = ""
//        UIView.animateWithDuration(0.225, animations: { () -> Void in
//            self.cameraStatus.alpha = 0.0
//            self.cameraPreview.alpha = 1.0
//            self.cameraCapture.alpha = 1.0
//            self.cameraCaptureShadow.alpha = 0.4;
//        })
//    }
//    
//    func cameraSessionDidStop() {
//        self.cameraStatus.text = "Camera Stopped"
//        UIView.animateWithDuration(0.225, animations: { () -> Void in
//            self.cameraStatus.alpha = 1.0
//            self.cameraPreview.alpha = 0.0
//        })
//    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
